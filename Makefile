CARTONEXE=carton exec

dependencies-install:
	carton install

build:
	${CARTONEXE} -- perl Build.PL
	${CARTONEXE} -- ./Build
	#${CARTONEXE} -- ./Build test
	${CARTONEXE} -- ./Build install

test-v:
	${CARTONEXE} -- prove -vl t/

test:
	${CARTONEXE} -- prove -l t/

test-entities:
	${CARTONEXE} -- prove -vl t/02_abstraction.t

test-agent:
	${CARTONEXE} -- prove -vl t/03_agent.t

test-collector:
	${CARTONEXE} -- prove -vl t/05_collector.t

test-manager:
	${CARTONEXE} -- prove -vl t/07_manager.t

test-introspection:
	${CARTONEXE} -- prove -vl t/30_introspection.t

test-fame:
	${CARTONEXE} -- prove -vl t/50_fame.t

bump-version:
	${CARTONEXE} perl-reversion -bump

build-readme:
	pod2readme --format 'markdown' lib/Art/World.pm README.md

build-tiny:
	${CARTONEXE} mbtiny regenerate
	${CARTONEXE} mbtiny dist
	#build-readme

upload:
	${CARTONEXE} mbtiny upload

# When this doesn't work it is because the ${{NEXT}} thing hasn't been replaced by a proper version in Changes file
# Also all those should be better executed through carton
# release: bump-version build-tiny upload

clean:
	rm -rf ./.build
	rm -f ./MYMETA.* ./README ./README.md.bak

names:
	@echo 'Generating for you...'
	@${CARTONEXE} -- script/names.pl
