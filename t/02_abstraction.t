use Test::More;
use Art::World;
use feature qw( postderef );
no warnings qw( experimental::postderef );

my $aw = Art::World->new_playground( name => 'World' );
ok $aw, 'The world is created';

my $art_concept = Art::World->new_idea(
  discourse => 'I have ideas. Too many ideas. It stimulates my imagination.',
  idea => 'idea',
  name => 'Yet Another Idea',
  process => [],
  project => 'project',
  time => 5,
 );

ok $art_concept->does('Art::World::Abstraction'), 'Art does role Abstraction';
is $art_concept->idea, 'idea';
is $art_concept->_imagination->[0], 'idea', 'Idea got correctly added to the imagination';

$art_concept->idea('demotivated');

is $art_concept->_imagination->[1], 'demotivated', 'Second idea correctly got added to the imagination';
ok scalar $art_concept->_imagination->@* eq 2, 'We have a couple of ideas in the imagination';

done_testing;
